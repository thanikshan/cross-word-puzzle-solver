//package finalAssignment4;

class WordSpace {
	private int length;
	private Point point;
	private boolean horizontal;

	WordSpace(int length, Point point, boolean horizontal) {
		this.length = length;
		this.point = point;
		this.horizontal = horizontal;
	}

	public int getLength() {
		return length;
	}

	public Point getPoint() {
		return point;
	}

	public boolean isHorizontal() {
		return horizontal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (horizontal ? 1231 : 1237);
		result = prime * result + length;
		result = prime * result + ((point == null) ? 0 : point.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WordSpace other = (WordSpace) obj;
		if (horizontal != other.horizontal)
			return false;
		if (length != other.length)
			return false;
		if (point == null) {
			if (other.point != null)
				return false;
		} else if (!point.equals(other.point))
			return false;
		return true;
	}

	
	
}
