//package finalAssignment4;

import java.util.Set;

class Puzzle {
	private char[][] puzzle;
	private int rows, columns;
	private Set<String> words;
	private Set<WordSpace> horizontalWordSpaces;
	private Set<WordSpace> verticalWordSpaces;

	Puzzle(int rows, int columns, Set<String> words, Set<WordSpace> horizontalWordSpaces,
			Set<WordSpace> verticalWordSpaces) {
		puzzle = new char[rows][columns];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				puzzle[i][j] = '-';
			}
		}
		this.horizontalWordSpaces = horizontalWordSpaces;
		this.verticalWordSpaces = verticalWordSpaces;
		this.rows = rows;
		this.columns = columns;
		this.words=words;
	}

	public int getRows() {
		return rows;
	}

	public int getColumns() {
		return columns;
	}

	public char[][] getPuzzle() {
		return puzzle;
	}

	public Set<String> getWords() {
		return words;
	}

	public Set<WordSpace> getHorizontalWordSpaces() {
		return horizontalWordSpaces;
	}

	public Set<WordSpace> getVerticalWordSpaces() {
		return verticalWordSpaces;
	}

}
