//package finalAssignment4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;

public class FillInPuzzle {
	public static Puzzle puzzle;
	public static int count;

	// method to load the puzzle
	public boolean loadPuzzle(BufferedReader stream) {
		try {
			String tempLine = stream.readLine();
			int rows = Integer.parseInt(tempLine.split("\\s+")[1]);
			int columns = Integer.parseInt(tempLine.split("\\s+")[0]);
			int noOfWords = Integer.parseInt(tempLine.split("\\s+")[2]);
			Set<WordSpace> horizontalSpaces = new HashSet<>();
			Set<WordSpace> verticalSpaces = new HashSet<>();
			Set<String> words = new HashSet<>();
			for (int i = 0; i < noOfWords; i++) {
				String[] line = stream.readLine().split("\\s+");
				int row = Integer.parseInt(line[1]);
				int column = Integer.parseInt(line[0]);
				int length = Integer.parseInt(line[2]);
				boolean horizontal = line[3].equals("h");
				Point point = new Point(row, column);
				if (horizontal) {
					horizontalSpaces.add(new WordSpace(length, point, true));
				} else {
					verticalSpaces.add(new WordSpace(length, point, false));
				}
			}
			for (int j = 0; j < noOfWords; j++) {
				String temp = stream.readLine().toLowerCase();
				if (temp.length() > rows || temp.length() > columns) {
					System.out.println("Cannot solve the puzzle with the current word length");
					return false;
				} else {
					if (words.contains(temp)) {
						System.out.println("Duplicate word in the set of words.Cannot proceed to solve the puzzle");
						return false;
					} else {
						words.add(temp);
					}
				}
				// words.add(stream.readLine());

			}
			puzzle = new Puzzle(rows, columns, words, horizontalSpaces, verticalSpaces);
			stream.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// method to call the solver method
	public boolean solve() {
		if (puzzle != null) {
			boolean flag = solver(puzzle.getPuzzle(), puzzle.getWords(), puzzle.getHorizontalWordSpaces(),
					puzzle.getVerticalWordSpaces(),puzzle.getRows(),puzzle.getColumns());
			if (!flag) {
				System.out.println("Cannot solve");
			}
			return flag;
		} else {
			return false;
		}

	}

	// method to solve the puzzle
	private static boolean solver(char[][] puzzlee, Set<String> words, Set<WordSpace> hSpaces, Set<WordSpace> vSpaces,int r,int c) {
		if (hSpaces.isEmpty() && vSpaces.isEmpty()) {
			return true;
		} 
		else if (!hSpaces.isEmpty()) { 
			WordSpace hSpace = hSpaces.iterator().next();
			int length = hSpace.getLength();
			Set<String> newWords = new HashSet<>();
			newWords.addAll(words);
			for (String word : words) {
				if (word.length() == length) {
					if (canFill(puzzlee, hSpace.getPoint(), true, word)) {
						char[][] nPuzzle = new char[r][c];
						clone(nPuzzle, puzzlee, r, c);
						fill(nPuzzle, hSpace.getPoint(), true, word);
						Set<WordSpace> newHSpaces = new HashSet<>();
						newWords.addAll(words);
						newWords.remove(word);
						newHSpaces.addAll(hSpaces);
						newHSpaces.remove(hSpace);
						boolean temp= solver(nPuzzle, newWords, newHSpaces, vSpaces,r,c);
						if (temp) {
							clone(puzzlee, nPuzzle, r, c);
							return temp;
						}
						else {
							count = count + 1;
						}
					}
				}
			}
			return false;
		} else {
			WordSpace vSpace = vSpaces.iterator().next();
			int length = vSpace.getLength();
			Set<String> newWords = new HashSet<>();
			newWords.addAll(words);
			for (String word : words) {
				if (word.length() == length) {
					if (canFill(puzzlee, vSpace.getPoint(), false, word)) {
						char[][] nPuzzle = new char[r][c];
						clone(nPuzzle, puzzlee, r, c);
						fill(nPuzzle, vSpace.getPoint(), false, word);
						Set<WordSpace> newVSpaces = new HashSet<>();
						newWords.addAll(words);
						newWords.remove(word);
						newVSpaces.addAll(vSpaces);
						newVSpaces.remove(vSpace);
						boolean temp= solver(nPuzzle, newWords, hSpaces, newVSpaces,r,c);
						if (temp) {
							clone(puzzlee, nPuzzle, r, c);
							return temp;
						}
						else {
							count = count + 1;
						}
					}
				}
			}
			return false;

		}
	}

	// method to determine the choices made
	public int choices() {
		System.out.println(count);
		return count;
	}

	// method to print the solved puzzle to a file
	public void print(PrintWriter outstream) {
		try {
			for (int i = puzzle.getRows() - 1; i >= 0; i--) {
				for (int j = 0; j < puzzle.getColumns(); j++) {
					char printPuzzle[][] = puzzle.getPuzzle();
					if (!(printPuzzle[i][j] == '-')) {
						outstream.write(printPuzzle[i][j] + " ");
					} else {
						outstream.write(" " + " ");
					}
				}
				outstream.write("\n");
			}
			outstream.close();
		} catch (Exception e) {
			System.out.println("Cannot print to a file with out data");
		}
	}

	// method to print the solved puzzle to the console
	private static void print(char[][] puzzle, int rows, int columns) {
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				System.out.print(puzzle[i][j] + "  ");
			}
			System.out.println();
		}
	}

	// method to check whether the word can be filled or not
	private static boolean canFill(char[][] puzzle, Point point, boolean horizontal, String word) {
		if (horizontal) {
			for (int i = 0, j = point.getColumn(); i < word.length(); i++, j++) {
				char cur = puzzle[point.getRow()][j];
				if (cur != '-' && cur != word.charAt(i)) {
					return false;
				}
			}
		} else {
			for (int i = 0, j = point.getRow(); i < word.length(); i++, j--) {
				char cur = puzzle[j][point.getColumn()];
				if (cur != '-' && cur != word.charAt(i)) {
					return false;
				}
			}
		}
		return true;
	}

	// method to fill the word into the puzzle
	private static void fill(char[][] puzzle, Point point, boolean horizontal, String word) {
		if (horizontal) {
			for (int i = 0, j = point.getColumn(); i < word.length(); i++, j++) {
				puzzle[point.getRow()][j] = word.charAt(i);
			}
		} else {
			for (int i = 0, j = point.getRow(); i < word.length(); i++, j--) {
				puzzle[j][point.getColumn()] = word.charAt(i);
			}
		}
	}
	
	//method to clone the puzzle which is used to refer the previous state in the recursion
	private static char[][] clone(char[][] source, char[][] destination, int rows, int columns) {
		for (int i = 0; i < rows; i++) {
			for (int j = 0;j < columns; j++) {
				source[i][j] = destination[i][j];
			}
		}
		return destination;
	}
}
